package socket_client

import (
    "log"
    "strings"
    "github.com/zhouhui8915/go-socket.io-client"
    "os"
)

var filename = "pipeline"

type SocketClient struct {
    ClientId *string
    ClientSecret *string
    AppId *string
    NodeId *string
    msgClientInfo *string
    Sclient *socketio_client.Client
}

var REGISTER_NODE_EVENT = "REGISTER_NODE"

func Construct(nodeId string, appId string, clientId string, clientSecret string) *SocketClient {
    return &SocketClient{
        ClientId : &clientId,
        ClientSecret : &clientSecret,
        AppId : &appId,
        NodeId: &nodeId}
}

func (sc *SocketClient) MsgClientInfoInit(nodeId string, clientId string, clientSecret string, appId string) {
    msgInfo := "{ \"clientId\" : \"" + clientId + "\",\n\"clientSecret\":  \"" + clientSecret + "\",\n\"appId\": \"" + appId + "\",\n\"nodeId\": \"" + nodeId + "\" }"
    sc.msgClientInfo = &msgInfo
}

func (sc *SocketClient) RegisterClient() {
    msg_type := REGISTER_NODE_EVENT

    msgContent := "{\"TYPE\" : \""+ msg_type +"\"}"
    
    msg := sc.CreateMsg(msgContent)
    sc.Sclient.Emit("register", msg)
}

func (sc *SocketClient) SendMsg(msg_type string, content string) {
    msgContent := "{\"TYPE\" : \""+ msg_type +"\", \"content\":" + content + "}"
    
    msg := sc.CreateMsg(msgContent)
    sc.Sclient.Emit("message", msg)
}

func (sc *SocketClient) CreateMsg(msgContent string) string {
    returnMsg := "{\"clientInfo\": " + *sc.msgClientInfo + ","
    returnMsg = returnMsg + "\n\"content\":" + strings.Trim(msgContent, "\n ") + "}"
    return returnMsg
}

func (sc *SocketClient) Init(nodeId string, clientId string, clientSecret string, appId string) {
    sc.MsgClientInfoInit(nodeId, clientId, clientSecret, appId)
}

func (sc *SocketClient) ConnectToServer(host string, port string) {
    opts := &socketio_client.Options{
		Transport: "websocket",
    }
    
    uri := "http://" + host + ":" + port
    
    var err error
	sc.Sclient, err = socketio_client.NewClient(uri, opts)

	if err != nil {
		return
    }

    sc.SocketClientHandlersInit()
}

func (sc *SocketClient) SocketClientHandlersInit() {
    sc.Sclient.On("error", func() {
		log.Printf("on error\n")
	})
	sc.Sclient.On("connection", func() {
		log.Printf("on connect\n")
	})
	sc.Sclient.On("message", func(msg string) {
		log.Printf("on message:%v\n", msg)
	})
	sc.Sclient.On("disconnection", func() {
        os.Exit(3)
    })
}
