# Log Client

This is a client to install on the web api server. It watches for log files and sends all changes on the log to the provider server


## Compile


```
. ./compile.sh
```



## Run


```
bin/start
```
