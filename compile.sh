# create bin directory
mkdir bin
cd bin

# get dependencies
go get github.com/howeyc/fsnotify
go get github.com/zhouhui8915/go-socket.io-client
go get github.com/shirou/gopsutil/...

# compile old version
# go build ../socket-client.go
# go build ../file-listener.go
# go build ../mulesoft-number-apps-and-domains.go
# go build ../start.go

# compile new version
go build ../servly-client.go

cd ..
