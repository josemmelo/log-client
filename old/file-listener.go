package main

import (
    "log"
    "github.com/howeyc/fsnotify"
    "io/ioutil"
    "strings"
    "fmt"
    "bytes"
    "os"
    "encoding/json"
    "regexp"
)

var filesChanges map[string]int
var filename = "pipeline"
var storageFile = "fileStorage"
var lastSent = ""
var regexToUse = "(?P<Date>[0-9-]+)([ ]+)(?P<Time>[0-9:,]+)([ ]+)(?P<App>[[a-zA-Z0-9_\\-\\[,\\].]+])([ ]+)(?P<Type>[A-Z]+)([ ]+)(?P<Class>[a-zA-Z.]+)([ ]+)-([ ]+)(?P<Msg>[a-zA-Z0-9_\\-\\[,\\]./\\n\\r\\t ]+)"

func main() {
    loadPersistedData()

    listenToFiles()
}

func logParts(logString string) []string{
    r, _ := regexp.Compile(regexToUse)
    return r.FindStringSubmatch(logString)
}

func getLogMsg(loginfo []string) string{

    values := map[string]string{"date": loginfo[1], "time": loginfo[3], "app":  loginfo[5], "type": loginfo[7], "class" : loginfo[9] , "msg": loginfo[12]}

    jsonBody, _ := json.Marshal(values)

    return string(jsonBody)
}

func listenToFiles() {

    watcher, err := fsnotify.NewWatcher()
    args := os.Args[1:]
    watcher.RemoveWatch("./")

    for _, element := range args {
        watcher.Watch(element)
    }

    if err != nil {
        log.Fatal(err)
    }

    done := make(chan bool)

    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                fmt.Println(ev)
                if(ev.IsModify()) {
                    file := getFilename(ev.Name)

                    if(file != filename && strings.Contains(file, "/")) {
                        file = "./" + file

                        dat, err := ioutil.ReadFile(file)
                        check(err)

                        lastLineStored := filesChanges[file]
                        changesDone := ""

                        numberLines := strings.Count(string(dat), "\n")
                        changesDone = getChangesDone(string(dat), lastLineStored)
                        filesChanges[file] = numberLines

                        logPartsFromString := logParts(changesDone)

                        fmt.Println(logPartsFromString)

                        if len(logPartsFromString) > 0 {
                            sendToSocketClientString := "{\"TYPE\" : \"LOG_MSG\", \"filename\":\"" + file + "\", \"content\":" + getLogMsg(logPartsFromString) + "}"
                            if strings.Compare(lastSent, sendToSocketClientString) != 0 {
                                lastSent = sendToSocketClientString
                                err = ioutil.WriteFile(filename, []byte(sendToSocketClientString), 0644)
                                fmt.Println(sendToSocketClientString)
                            }
                        }

                        persistDataToFile()
                    }
                }

                if(ev.IsDelete() || ev.IsRename()){
                    file := "./" + getFilename(ev.Name)
                    fmt.Println(file)
                    delete(filesChanges, file)

                    persistDataToFile()
                }
            case err := <-watcher.Error:
                log.Println("error:", err)
            }
        }
    }()

    err = watcher.Watch("./")
    if err != nil {
        log.Fatal(err)
    }

    <-done

    watcher.Close()
}

func getChangesDone(fullText string, line int) string {
    fileLines := strings.Split(fullText, "\n")

    var buffer bytes.Buffer

    for i := line; i < len(fileLines); i++ {
        buffer.WriteString(fileLines[i] + "\n")
    }

    return buffer.String()
}

func persistDataToFile(){
    saveArray , _ := json.Marshal(filesChanges)

    err := ioutil.WriteFile(storageFile, []byte(saveArray), 0644)
    if err != nil {
        fmt.Println(err)
    }
}

func getFilename(file string) string {
    filename := strings.Replace(file, "\"", "", -1)
    filename = strings.Replace(file, " ", "", -1)
    return filename
}

func loadPersistedData(){
    filesChanges = make(map[string]int)
    dat, _ := ioutil.ReadFile(storageFile)

        fmt.Println("err")
    err := json.Unmarshal(dat, &filesChanges)
        fmt.Println(filesChanges)
    if err != nil {
        filesChanges = make(map[string]int)
    }
        fmt.Println(filesChanges)
}


func check(e error) {
    if e != nil {
        log.Println(e)
    }
}
