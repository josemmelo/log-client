package main

import (
    "log"
    "github.com/howeyc/fsnotify"
    "io/ioutil"
    "strings"
    "fmt"
    "os"
    "strconv"
)

var filename = "pipeline"

var lastSent = ""


func main() {
    listenToFiles()
}

func listenToFiles() {
    args := os.Args[1:]

    watcher, err := fsnotify.NewWatcher()
    watcher.RemoveWatch("./")

    if err != nil {
        log.Fatal(err)
    }

    appsFolder := args[0]
    watcher.Watch(appsFolder)

    domainsFolder := args[1]
    watcher.Watch(domainsFolder)

    done := make(chan bool)

    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                if ev.Name == "" {
                    continue
                }
                appsFiles,_ := ioutil.ReadDir(appsFolder)
                domainsFiles,_ := ioutil.ReadDir(domainsFolder)

                runningApps := countRunning(appsFiles)
                totalApps := totalDeployed(appsFiles)
                runningDomains := countRunning(domainsFiles)
                totalDomains := totalDeployed(domainsFiles)

                fmt.Println(runningApps)
                sendToSocketClientString := "{\"TYPE\":\"MULESOFT_RUNNING_APPS\",\n\"running_apps\":" + strconv.Itoa(runningApps) + ",\n\"running_domains\":" + strconv.Itoa(runningDomains) + ",\n"
                sendToSocketClientString = sendToSocketClientString + "\"total_apps\":" + strconv.Itoa(totalApps) +    ",\"total_domains\":" + strconv.Itoa(totalDomains) + "}"

                if strings.Compare(lastSent, sendToSocketClientString) != 0 {
                    lastSent = sendToSocketClientString
                    err = ioutil.WriteFile(filename, []byte(sendToSocketClientString), 0644)
                }

            case err := <-watcher.Error:
                log.Println("error:", err)
            }
        }
    }()

    if err != nil {
        log.Fatal(err)
    }

    <-done

    watcher.Close()
}

func countRunning(files []os.FileInfo) int {
    count := 0
    for _, file := range files {
        if(strings.Contains(file.Name(), "-anchor.txt")) {
            count = count + 1
        }
	}
    return count
}


func totalDeployed(files []os.FileInfo) int {
    count := 0
    for _, file := range files {
        if(file.IsDir()) {
            count = count + 1
        }
	}
    return count
}

func check(e error) {
    if e != nil {
        log.Println(e)
    }
}
