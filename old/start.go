package main

import (
    "fmt"
    "os/exec"
    "io/ioutil"
    "encoding/xml"
    "encoding/json"
    "os"
    "strings"
    "net/http"
    "bytes"
    "./os_stats"
)

type Configuration struct {
    ClientId string `xml:"client_id"`
    ClientSecret string `xml:"client_secret"`
    AppId string `xml:"app_id"`
}

var c Configuration

var socketClientCmd *exec.Cmd
var fileListenerCmd *exec.Cmd
var numberAppsAndDomainsManagerCmd *exec.Cmd

var authorizeURL = "http://servly.dev:8000/api/users/authorize"
var appID = ""

func main() {
    os_stats.GetHardwareData()
    
    getClientCredentials()

    runFileListener()

    runSocketClient()

    runMuleRunningAppsAndDomains()
    
    waitForCommands()
}


func getClientCredentials() {
    // get credentials from file
    credDatXML, err := ioutil.ReadFile("./credentials.xml")

    if err != nil {
        fmt.Println("Credentials not found.")
        os.Exit(1)
    }

    xml.Unmarshal(credDatXML, &c)

    // validate credentials
    values := map[string]string{"client_id": c.ClientId, "client_secret": c.ClientSecret}

    appID = c.AppId

    jsonBody, _ := json.Marshal(values)

    resp, err1 := http.Post(authorizeURL, "application/json", bytes.NewBuffer(jsonBody))

    if err1 != nil {
        fmt.Println("Error")
        os.Exit(1)
    }

    if resp.StatusCode == 401 {
        os.Exit(1)
    }
}

func waitForCommands(){
    err := socketClientCmd.Wait()
    if err != nil {
        fmt.Println("Socket error")
    }
    err1 := fileListenerCmd.Wait()
    if err1 != nil {
        fmt.Println("File listener error")
    }
    err2 := numberAppsAndDomainsManagerCmd.Wait()
    if err2 != nil {
        fmt.Println("Mule apps manager error")
    }

}

func runFileListener() {
    args := os.Args[1:]

    directoriesToWatch := strings.Split(args[0], ",")

    fileListenerCmd = exec.Command("./file-listener", directoriesToWatch...)

    err1 := fileListenerCmd.Start()
    if err1 != nil {
      fmt.Println(err1)
    }
}

func runSocketClient() {
    args := []string{c.ClientId, c.ClientSecret, c.AppId}

    socketClientCmd = exec.Command("./socket-client", args...)

    err := socketClientCmd.Start()
    if err != nil {
      fmt.Println(err)
    }
}

func runMuleRunningAppsAndDomains() {
    args := []string{"../../apps", "../../domains"}

    numberAppsAndDomainsManagerCmd = exec.Command("./mulesoft-number-apps-and-domains", args...)

    err := numberAppsAndDomainsManagerCmd.Start()
    if err != nil {
      fmt.Println(err)
    }
}
