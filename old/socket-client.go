package main

import (
    "log"
    "github.com/howeyc/fsnotify"
    "io/ioutil"
    "strings"
    "github.com/zhouhui8915/go-socket.io-client"
    "os"
    "fmt"
)

var client *socketio_client.Client
var filename = "pipeline"


var clientId string
var clientSecret string
var appId string
var msgClientInfo string


func main() {
    args := os.Args[1:]

    clientId = args[0]
    clientSecret = args[1]
    appId = args[2]

    msgClientInfo = "{ \"clientId\" : \"" + clientId + "\",\n\"clientSecret\":  \"" + clientSecret + "\",\n\"appId\": \"" + appId + "\" }"

    socketIOClient()

    listenToFiles()
}


func listenToFiles() {
    watcher, err := fsnotify.NewWatcher()
    if err != nil {
        log.Fatal(err)
    }

    done := make(chan bool)

    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                fmt.Println(ev)
                if(ev.IsModify() && ev.IsAttrib()) {
                    file := ev.Name
                    file = strings.Replace(file, "\"", "", -1)
                    file = strings.Replace(file, " ", "", -1)
                    if(file == filename) {
                        file = "./" + file

                        dat, err := ioutil.ReadFile(file)
                        check(err)
                        fmt.Println(string(dat))

                        msg := createMsg(string(dat))

                        client.Emit("message", msg )
                    }
                }
            case err := <-watcher.Error:
                log.Println("error:", err)
            }
        }
    }()

    err = watcher.Watch("./")
    if err != nil {
        log.Fatal(err)
    }

    <-done

    watcher.Close()
}


func check(e error) {
    if e != nil {
        log.Println(e)
    }
}

func createMsg(msgContent string) string {
    returnMsg := "{\"clientInfo\": " + msgClientInfo + ","
    returnMsg = returnMsg + "\n\"content\":" + strings.Trim(msgContent, "\n ") + "}"
    return returnMsg
}


func connectToServer() {
    opts := &socketio_client.Options{
		Transport: "websocket",
	}
	uri := "http://localhost:5000"

	client1, err := socketio_client.NewClient(uri, opts)

	if err != nil {
		return
	}

    client = client1
}

func socketIOClient() {

    connectToServer()

	client.On("error", func() {
		log.Printf("on error\n")
	})
	client.On("connection", func() {
		log.Printf("on connect\n")
	})
	client.On("message", func(msg string) {
		log.Printf("on message:%v\n", msg)
	})
	client.On("disconnection", func() {
        os.Exit(3)
	})
}
