package main

import (
    "fmt"
    "io/ioutil"
    "encoding/xml"
    "encoding/json"
    "os"
    "strings"
    "net/http"
    "bytes"
	"./os_stats"
	"./file_watcher"
	"./file_watcher/mule_apps_domains"
    "./socket_client"
    "time"
    "github.com/howeyc/fsnotify"
)

var socketClient *socket_client.SocketClient
var fileWatcher file_watcher.FileWatcher

type Configuration struct {
    ClientId string `xml:"client_id"`
    ClientSecret string `xml:"client_secret"`
    AppId string `xml:"app_id"`
    NodeId string `xml:"node_id"`
    Domain string `xml:"domain"`
}

type ApplicationInformation struct {
    Regex string `json:"regex"`
    ActiveServices []string `json:"active_services"`
    LogLevel []string `json:"log_level"`    
}

var applicationInfo ApplicationInformation
var c Configuration
var runningOps []string

var authorizeURL = "/api/users/authorize"
var applicationInformationURL = "/api/applications/{id}/information"
var appID = ""
var port = "5000"

var NODE_MONITOR_TIME = 1
var UPDATE_FUNCTIONALITIES_TIME = 60
var OS_MONITOR_MSG_TYPE = "OS_MONITOR"

func main() {
    getClientCredentials()
    
    startCustomFunctionalities()

    startCustomFunctionalitiesLookup()

    wait()
}

func wait() {
    watcher, err := fsnotify.NewWatcher()
    
    if err != nil {
        fmt.Println(err)
    }

    done := make(chan bool)
    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                fmt.Println(ev)
            case err := <-watcher.Error:
                fmt.Println("error:", err)
            }
        }
    }()
    <-done
    watcher.Close()
}

func startCustomFunctionalitiesLookup() {
    customTicker := time.NewTicker(time.Duration(UPDATE_FUNCTIONALITIES_TIME) * time.Second)
    done := make(chan struct{})
    go func() {
        for {
           select {
            case <- customTicker.C:            
                startCustomFunctionalities()
            case <- done:
                return
            }
        }
     }()
}

func startCustomFunctionalities() {
    getApplicationInformation()

    runOptativeFunctionalities()    
}

func getApplicationInformation(){

    fmt.Println("APPLICATION INFORMATION")
    
    client := &http.Client{}    
    applicationInformationURL = strings.Replace(applicationInformationURL, "{id}", c.AppId, -1)
    req, _ := http.NewRequest("GET", applicationInformationURL, nil)
    req.Header.Add("clientid", c.ClientId)
    req.Header.Add("clientsecret", c.ClientSecret)

    resp, _ := client.Do(req)
    
    defer resp.Body.Close()

    if resp.StatusCode == http.StatusOK {
        bodyBytes, _ := ioutil.ReadAll(resp.Body)

        err := json.Unmarshal(bodyBytes, &applicationInfo)
        if err != nil {
            os.Exit(2)
        }
    }
}

func contains(arr []string, str string) bool {
    for _, a := range arr {
       if a == str {
          return true
       }
    }
    return false
 }
 
func mulesoft_apps_domains(){
    fmt.Println("START MULESOFT_APPS_DOMAINS")
    mule_apps_domains.MonitorNumberMuleAppsAndDomains(socketClient, "../../apps", "../../domains")
}

func node_monitor(){
    fmt.Println("START NODE_MONITOR")
    ticker := time.NewTicker(time.Duration(NODE_MONITOR_TIME) * time.Second)
    quit := make(chan struct{})
    go func() {
        for {
           select {
            case <- ticker.C:
                hardwareStats := os_stats.GetHardwareData()
                hardwareStatsJSON, _ := json.Marshal(*hardwareStats)
                socketClient.SendMsg(OS_MONITOR_MSG_TYPE, string(hardwareStatsJSON))
            case <- quit:
                ticker.Stop()
                return
            }
        }
    }()
}

func logs_monitor(){
    fmt.Println("START LOGS_MONITOR")
    
    args := os.Args[1:]

    directoriesToWatch := strings.Split(args[0], ",")

    file_watcher.StartWatching(socketClient, directoriesToWatch, applicationInfo.Regex, applicationInfo.LogLevel)
}

func runOptativeFunctionalities(){
    for _, value := range applicationInfo.ActiveServices {
        if contains(runningOps, value) == true {
            continue
        }

        if value == "node_monitor" {
            go node_monitor()
        }
        if value == "mulesoft_apps_domains" {
            go mulesoft_apps_domains()
        }
        if value == "logs_monitor" {
            go logs_monitor()
        }
        
        runningOps = append(runningOps, string(value))
    }
}

func getClientCredentials() {
    // get credentials from file
    credDatXML, err := ioutil.ReadFile("./credentials.xml")

    if err != nil {
        fmt.Println("Credentials not found.")
        os.Exit(1)
    }

    xml.Unmarshal(credDatXML, &c)

    // add domain to url
    authorizeURL = "http://" + c.Domain + authorizeURL
    applicationInformationURL = "http://" + c.Domain + applicationInformationURL

    // validate credentials
    values := map[string]string{"client_id": c.ClientId, "client_secret": c.ClientSecret}

    appID = c.AppId

    jsonBody, _ := json.Marshal(values)

    resp, err1 := http.Post(authorizeURL, "application/json", bytes.NewBuffer(jsonBody))

    if err1 != nil {
        fmt.Println("Error on authorization");
        os.Exit(1)
    }

    if resp.StatusCode == 401 {
        os.Exit(1)
    }
    
    socketClient = socket_client.Construct(c.NodeId, c.AppId, c.ClientId, c.ClientSecret)
    
    socketClient.Init(c.NodeId, c.ClientId, c.ClientSecret, c.AppId)
    socketClient.ConnectToServer(c.Domain, port)
    
    socketClient.RegisterClient()
}

