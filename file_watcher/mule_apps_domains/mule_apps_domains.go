package mule_apps_domains

import (
    "log"
    "github.com/howeyc/fsnotify"
    "io/ioutil"
    "strings"
    "fmt"
    "os"
    "strconv"
    "../../socket_client"
)

var lastSent = ""
var filename = "pipeline"

func MonitorNumberMuleAppsAndDomains(socketClient *socket_client.SocketClient, appsFolder string, domainsFolder string) {
    watcher, err := fsnotify.NewWatcher()
    watcher.RemoveWatch("./")

    if err != nil {
        log.Fatal(err)
    }

    watcher.Watch(appsFolder)
    watcher.Watch(domainsFolder)

    done := make(chan bool)

    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                if ev.Name == "" {
                    continue
                }
                appsFiles,_ := ioutil.ReadDir(appsFolder)
                domainsFiles,_ := ioutil.ReadDir(domainsFolder)

                runningApps := countRunning(appsFiles)
                totalApps := totalDeployed(appsFiles)
                runningDomains := countRunning(domainsFiles)
                totalDomains := totalDeployed(domainsFiles)

                fmt.Println(runningApps)
                sendToSocketClientString := "\"running_apps\":" + strconv.Itoa(runningApps) + ",\n\"running_domains\":" + strconv.Itoa(runningDomains) + ",\n"
                sendToSocketClientString = sendToSocketClientString + "\"total_apps\":" + strconv.Itoa(totalApps) +    ",\"total_domains\":" + strconv.Itoa(totalDomains)

                if strings.Compare(lastSent, sendToSocketClientString) != 0 {
                    lastSent = sendToSocketClientString
                    err = ioutil.WriteFile(filename, []byte(sendToSocketClientString), 0644)

                    socketClient.SendMsg("MULESOFT_RUNNING_APPS", sendToSocketClientString)
                }

            case err := <-watcher.Error:
                log.Println("error:", err)
            }
        }
    }()

    if err != nil {
        log.Fatal(err)
    }

    <-done

    watcher.Close()
}

func countRunning(files []os.FileInfo) int {
    count := 0
    for _, file := range files {
        if(strings.Contains(file.Name(), "-anchor.txt")) {
            count = count + 1
        }
	}
    return count
}


func totalDeployed(files []os.FileInfo) int {
    count := 0
    for _, file := range files {
        if(file.IsDir()) {
            count = count + 1
        }
	}
    return count
}