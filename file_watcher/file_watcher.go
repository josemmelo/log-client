package file_watcher

import (
    "log"
    "github.com/howeyc/fsnotify"
    "io/ioutil"
    "strings"
    "fmt"
    "bytes"
    "encoding/json"
    "regexp"
    "../socket_client"
)

type FileWatcher struct {
    
}

var socket *socket_client.SocketClient
var filesChanges map[string]int
var filename = "pipeline"
var storageFile = "fileStorage"
var lastSent = ""
var regexToUse = "(?P<Date>[0-9-]+)([ ]+)(?P<Time>[0-9:,]+)([ ]+)(?P<App>[[a-zA-Z0-9_\\-\\[,\\].]+])([ ]+)(?P<Type>[A-Z]+)([ ]+)(?P<Class>[a-zA-Z.]+)([ ]+)-([ ]*)(?P<Msg>[a-zA-Z0-9_\\-\\[,\\]./\\n\\r\\t\\*:@\\(\\-\\)\\'= ]+)"
var logLevelToFilter []string

func StartWatching(socketClient *socket_client.SocketClient, args []string, regex string, logLevel []string) {
    socket = socketClient
    
    logLevelToFilter = logLevel

    loadPersistedData()

    listenToFiles(args)

    if regex != "" {
        regexToUse = regex
    }
}

func logParts(logString string) map[string]string{
    r, _ := regexp.Compile(regexToUse)
    n1 := r.SubexpNames()
    r2 := r.FindStringSubmatch(logString)

    md := map[string]string{}
    for i, n := range r2 {
        fmt.Printf("%d. match='%s'\tname='%s'\n", i, n, n1[i])
        md[n1[i]] = n
    }
    
    return md
}

func getLogMsg(loginfo map[string]string) string{

    values := map[string]string{"date": loginfo["Date"], "time": loginfo["Time"], "app":  loginfo["App"], "type": loginfo["Type"], "class" : loginfo["Class"] , "msg": loginfo["Msg"]}

    exists, _ := in_array(values["type"], logLevelToFilter) 

    if exists {
        jsonBody, _ := json.Marshal(values)

        return string(jsonBody)
    }
    return ""    
}

func in_array(val string, array []string) (exists bool, index int) {
    exists = false
    index = -1;

    for i, v := range array {
        if val == v {
            index = i
            exists = true
            return
        }   
    }

    return
}

func listenToFiles(args []string) {
    watcher, err := fsnotify.NewWatcher()
    watcher.RemoveWatch("./")

    for _, element := range args {
        watcher.Watch(element)
    }

    if err != nil {
        log.Fatal(err)
    }

    done := make(chan bool)

    go func() {
        for {
            select {
            case ev := <-watcher.Event:
                fmt.Println(ev)
                if(ev.IsModify()) {
                    file := getFilename(ev.Name)

                    if(file != filename && strings.Contains(file, "/")) {
                        file = "./" + file

                        dat, err := ioutil.ReadFile(file)
                        check(err)

                        lastLineStored := filesChanges[file]

                        numberLines := strings.Count(string(dat), "\n")

                        if numberLines < lastLineStored {
                            lastLineStored = 0
                        }

                        uploadChanges(file, string(dat), lastLineStored)
                        //changesDone = getChangesDone(string(dat), lastLineStored)
                        filesChanges[file] = numberLines
                        
                        // logPartsFromString := logParts(changesDone)

                        // fmt.Println(logPartsFromString)
                        persistDataToFile()
                    }
                }

                if(ev.IsDelete() || ev.IsRename()){
                    file := "./" + getFilename(ev.Name)
                    fmt.Println(file)
                    delete(filesChanges, file)

                    persistDataToFile()
                }
            case err := <-watcher.Error:
                log.Println("error:", err)
            }
        }
    }()

    err = watcher.Watch("./")
    if err != nil {
        log.Fatal(err)
    }

    <-done

    watcher.Close()
}

func getChangesDone(fullText string, line int) string {
    fileLines := strings.Split(fullText, "\n")

    var buffer bytes.Buffer

    for i := line; i < len(fileLines); i++ {
        buffer.WriteString(fileLines[i] + "\n")
    }

    return buffer.String()
}

func uploadChanges(file string, fullText string, line int) {
    fileLines := strings.Split(fullText, "\n")

    for i := line; i < len(fileLines); i++ {
        logPartsFromString := logParts(fileLines[i])

        if len(logPartsFromString) > 0 {
            logMsg := getLogMsg(logPartsFromString)
            if logMsg != "" {
                sendToSocketClientString := "{\"TYPE\" : \"LOG_MSG\", \"filename\":\"" + file + "\", \"content\":" + logMsg + "}"
                if strings.Compare(lastSent, sendToSocketClientString) != 0 {
                    lastSent = sendToSocketClientString
                    //err = ioutil.WriteFile(filename, []byte(sendToSocketClientString), 0644)
                    //fmt.Println(sendToSocketClientString)

                    socket.SendMsg("LOG_MSG", getLogMsg(logPartsFromString))
                }
            }
            
        }
    }
}

func persistDataToFile(){
    saveArray , _ := json.Marshal(filesChanges)

    err := ioutil.WriteFile(storageFile, []byte(saveArray), 0644)
    if err != nil {
        fmt.Println(err)
    }
}

func getFilename(file string) string {
    filename := strings.Replace(file, "\"", "", -1)
    filename = strings.Replace(file, " ", "", -1)
    return filename
}

func loadPersistedData(){
    filesChanges = make(map[string]int)
    
    dat, err1 := ioutil.ReadFile(storageFile)
    
    if err1 != nil {
        filesChanges = make(map[string]int)
        return        
    }

    err := json.Unmarshal(dat, &filesChanges)
    
    fmt.Println("err")

    if err != nil {
        filesChanges = make(map[string]int)
    }
    
    fmt.Println("err")
}

func check(e error) {
    if e != nil {
        log.Println(e)
    }
}
