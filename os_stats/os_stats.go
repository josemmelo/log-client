package os_stats

import (
         "github.com/shirou/gopsutil/cpu"
         "github.com/shirou/gopsutil/disk"
         "github.com/shirou/gopsutil/host"
         "github.com/shirou/gopsutil/mem"
         "github.com/shirou/gopsutil/net"
         "runtime"
         "encoding/json"
         "fmt"
)

type HardwareData struct{
        RuntimeOS string
        VirtualMemory string
        DiskUsage string
        CpuUsage []float64
        Host string
        Interfaces string
}

func dealwithErr(err error) {
         if err != nil {
                 fmt.Println(err)
                 //os.Exit(-1)
         }
 }

func GetHardwareData() *HardwareData {

        runtimeOS := runtime.GOOS

        // memory
        vmStat, err := mem.VirtualMemory()
        dealwithErr(err)

        // disk - start from "/" mount point for Linux
        // might have to change for Windows!!
        // don't have a Window to test this out, if detect OS == windows
        // then use "\" instead of "/"

        diskStat, err := disk.Usage("/")
        dealwithErr(err)

        // cpu - get CPU number of cores and speed
        //cpuStat, err := cpu.Info()
        //dealwithErr(err)

        percentage, err := cpu.Percent(0, false)
        dealwithErr(err)

        // host or machine kernel, uptime, platform Info
        hostStat, err := host.Info()
        dealwithErr(err)

        // get interfaces MAC/hardware address
        interfStat, err := net.Interfaces()
        dealwithErr(err)

        out, err := json.Marshal(interfStat)
        if err != nil {
            panic (err)
        }
    
        osStat := &HardwareData{
                RuntimeOS : runtimeOS,
                VirtualMemory : vmStat.String(),
                DiskUsage : diskStat.String(),
                CpuUsage : percentage,
                Host: hostStat.String(),
                Interfaces: string(out)}

        return osStat
 }
